#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Adafruit_NeoPixel.h>
#include <stdlib.h>
#include <wiring_private.h>

#define N_LEDS 111
#define updateRate 500 // The minimal time between two requests in ms
#define PORT 80
#define BASE_URL "interlight-aarhus.herokuapp.com" //WITHOUT HTTP://

const char* ssid     = "vilduhavevirus";
const char* password = "jadakommeddet";

int fsr = A0;
int fsrMin = 500;

int fsrTimeStamp;
int timeStamp;
int interval;
int postInterval;
int herokuTimeStamp;
int updateTimestamp;
int randomInterval;
int randomTimestamp;
int readTimeStamp;
int readInterval;

String getmoved;
String d1pos = "a";
String d2pos = "b";
String prevPos;
String getAngleAndBeenSet;

int fsrS1;

boolean isRunning;
boolean hasBeenTrue;
boolean upOrDown;
boolean isSetInHeroku;
boolean hasRun;

int actUp = D1;
int actDown = D3;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(111, D6, NEO_GRB + NEO_KHZ800);

HTTPClient http;

void setup() {

  WiFi.persistent(false); //  Do this just to be on the safe side!!!!

  delay(10);
  wifiConnect();

  Serial.begin(9600); // to read and write to Heroku, a Serial port must be open

  timeStamp = 0; // timestamp for sending data from fsr sensor
  herokuTimeStamp = 0;
  interval = 1000; // the interval of the fsr check
  readInterval = 1500;
  readTimeStamp = millis() + 500;
  updateTimestamp = 0;
  randomTimestamp = 0;
  randomInterval = random(6000,15000);
  fsrS1 = 0;
  fsrTimeStamp = 0;
  isRunning = false;
  hasBeenTrue = false;
  upOrDown = true;
  isSetInHeroku = false;
  hasRun = false;

  getAngleAndBeenSet = "";
  prevPos = "";

  // Actuators input mode setup
  pinMode(actUp,OUTPUT);
  pinMode(actDown,OUTPUT);
  goNowhere();

  strip.begin();
  strip.setBrightness(200);
}

void loop() {
  wifiCheck(); //Maintain wifi connection
  yield(); //Let the ESPcore handle background tasks
  //if(d1pos == "a" && millis() > readTimeStamp + readInterval){
    //readTimeStamp = millis();
    setInHeroku();
    //} 
  if(isSetInHeroku && !hasRun){
    Serial.println("yup");
    readFromHeroku(getAngleAndBeenSet);
  }
  
  if(!isSetInHeroku) {
    fsrSignal(); // run FSR and trigger GET+POST & ACTS
  } 
  Serial.println(isSetInHeroku);
}

void setInHeroku(){
  getAngleAndBeenSet = get("/api/d1miniangle");
      //Serial.println("is set?:" + getAngleAndBeenSet.substring(getAngleAndBeenSet.indexOf('-')+1));
      //Serial.println("Angle: " + getAngleAndBeenSet.substring(0,getAngleAndBeenSet.indexOf('-')));
  String beenSet = getAngleAndBeenSet.substring(getAngleAndBeenSet.indexOf('-')+1);
  if(beenSet == "True" && !isSetInHeroku){
    Serial.print("yolo"); //DEBUG
    isSetInHeroku = true;
  } 
  if(isSetInHeroku && millis() > herokuTimeStamp + 10000){
    herokuTimeStamp = millis();
    if(beenSet == "False"){
      resetActuators();
    }
  }
}

// Runs the commands affiliated with the fsr signal
void fsrSignal(){
  String api_get;
  // READING FSR VALUES EVERY HALF SEC
  if(millis() > fsrTimeStamp + 7000){
    fsrTimeStamp = millis();
    fsrS1 = analogRead(fsr);
    //Serial.print(fsrS1);
    //Serial.print(", ");
  }
  // GETTING THE "IS FSR STEPPED UPON" DATA FROM HEROKU (to send forward) EVERY 4 SEC
  //if(millis() > timeStamp + interval){
    //Serial.println("whaaaaaat"); // DEBUG
    //timeStamp = millis();
    api_get = get("/api/fsrgetvalue");
  //}
  setHerokuData(fsrS1, api_get); // SENDING DATA TO HEROKU ABOUT FSR
  setServoandLED(fsrS1, api_get); // SETTING LEDS (blue/green) & ACTs
}

// SENDING DATA TO HEROKU IF FSR IS STEPPED UPON
void setHerokuData(int fsrSignal, String api_get){
  String postString = "";

  //if(millis() > postTimeStamp + interval){ // setting interval
    //postTimeStamp = millis();
    //Serial.println("test");
    if(fsrSignal>fsrMin){ // the number
      postString = "True" + d1pos;
      // TO GET INTERVAL BETWEEN JUMPS - IF WE HAD MORE FSR's
      // post(String(timeStamp), "/api/fsrtimestamp");
    } else {
      postString = "False";
    }
    //if(postString == "True" + d1pos || prevPos == "True" + d1pos){
      post(postString, "/api/fsrreadvalue");
    //}
    prevPos = api_get;
  //}
}

void setServoandLED(int fsrSignal, String api_get){
  String getmoved = api_get;
  boolean isSteppedUpon = false;
  // LEDS
  if(fsrSignal < fsrMin){
    chase(strip.Color(0,80,0));
  } else if(fsrSignal > fsrMin){
    isSteppedUpon = true;
    chase(strip.Color(80, 0, 0)); // Blue
  }

  if(getmoved == "True" + d2pos && d1pos == "a" || isSteppedUpon == true){
    hasBeenTrue = true;
  }
  // RAND INTERV. TO GET RAND POSITIONS OF THE BLOCKS
  if(millis() > randomTimestamp + randomInterval){
    randomTimestamp = millis();

    // SETTING THE NEXT RANDOM INTERVAL
    randomInterval = random(3000,12000);
    // IF ANOTHER BLOCK HAS BEEN STEPPED ON - REPR. BY "Trueb" (True + d2pos)
    // int fsrDifference = millis() - get("/api/getfsrtimestamp").toInt();

      // CHECKS IF BOTH PINS ARE SET TO NOT ACTIVE
    if(isRunning == false && hasBeenTrue == true){
      hasBeenTrue = false;
      isRunning = true;
      //Serial.println("CRACK OPEN A COLD ONE W THE BOISS");
      // IF r == 0 or 1, MOVE UP OR DOWN
      if(upOrDown){
        upOrDown = !upOrDown;
        goDown();
      } else if(!upOrDown) {
        upOrDown = !upOrDown;
        goUp();
      }
    } else { // IF ACTUATORS ARE RUNNING
      isRunning = false;
      goNowhere();
    }
  }
}

// READS DATA FROM WEB-APP IF HAS BEEN SET
void readFromHeroku(String angleAndSet){
  //String getheight = get("/api/d1miniheight"); NO NEED RIGHT NOW, AS WE CAN'T SET HEIGHT
  String getangle = angleAndSet.substring(0, angleAndSet.indexOf('-'));
  //int height = getheight.toInt(); AGAIN, CAN'T SET HEIGHT OF BOX
  int angle = getangle.toInt(); // CONVERTING ANGLE STR to INT
  int convertToMs = (angle*1000)/3; // CONVERTION: acts move 10 mm/s, with a stretch up to
  if(millis() > timeStamp + convertToMs && d1pos == "a"){
    timeStamp = millis();
    if(isRunning == false){
      isRunning = true;
      goUp();
    } else if(isRunning == true){
      isRunning = false;
      goNowhere();
      hasRun = true;
    }
  }
}

void resetActuators(){
  if(millis() > readTimeStamp + 14000){
    readTimeStamp = millis();
    if(!isRunning){
      goDown();
    } else if(isRunning){
      goNowhere();
      isSetInHeroku = false;
    }
  }

}

void goDown(){
  digitalWrite(actDown, LOW);
  digitalWrite(actUp, HIGH);
}

void goUp(){
  digitalWrite(actDown, HIGH);
  digitalWrite(actUp, LOW);
}

void goNowhere(){
  digitalWrite(actDown, HIGH);
  digitalWrite(actUp, HIGH);
}

// CREDIT TO PHILIP BURGESS @ learn.adafruit.com
static void chase(uint32_t c) {
 for(uint16_t i=0; i<N_LEDS; i++) {
     strip.setPixelColor(i  , c); // Draw new pixel
     strip.show();
 }
}

// QUDOS TO DANIEL GRAUNGAARD
void wifiConnect() {
  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void wifiCheck(){
  if (WiFi.status() != WL_CONNECTED) //if wifi is connected: do nothing.
  {
    int tickCount = 0;
    Serial.println("Wifi dropped. Retry in 60 seconds.");
    delay(60000); //wait 60 seconds
    Serial.println("Connecting");
    WiFi.begin(ssid, password); //reconnect

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.println(".");
      tickCount++;
      if (tickCount > 100) //after awaiting reconnection for 50 seconds
      {
        Serial.println("Wifi fail...");
        //This is where you end up if the connection was lost and unrecoverable
        while (1); //Endless loop...
      }
    }

    //This is the place to do something in case the connection was lost but fixed.

  }
}

String post(String payload, String url) {
    Serial.println("[POST BEGIN] ...");
    String response = "Request not allowed because of timelimit";
    if( millis() - updateTimestamp > updateRate) { // Safeguards against server timeouts
      updateTimestamp = millis();
      //Serial.println("[HTTP] POST begin...");
      // configure traged server and url
      http.begin(BASE_URL, PORT, url); //HTTP
      Serial.println("[POST] payload: " + payload);
      Serial.println("url: " + url);
      String status = String(http.POST(payload));
      response = http.getString();
      //Serial.println("Status: " + status);
      Serial.println("Response: " + response);
      http.end();
    }
    return response;
}

String get(String url) {
    Serial.println("[GET BEGIN] ...");
    String response = "Request not allowed because of timelimit";
    if( millis() - updateTimestamp > updateRate) { // Safeguards against server timeouts
        http.begin(BASE_URL, PORT, url); //HTTP
        // start connection and send HTTP header
        int httpCode = http.GET();
        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            //Serial.println("[HTTP] GET... code: " + String(httpCode));

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                response = http.getString();
                Serial.println("[HTTP] Response " + response);
            }
        } else {
            Serial.println("[HTTP] GET... failed, error: " + String(httpCode));
            response = "ERROR";
        }
        http.end();
      }
      return response;
}
