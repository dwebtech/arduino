# 1 "c:\\users\\tobias~1\\appdata\\local\\temp\\tmprzxnoh"
#include <Arduino.h>
# 1 "C:/Users/Tobias Lund Petersen/Documents/Arduino/arduino/src/fsr.ino"
#include <ESP8266WiFi.h>

#include <ESP8266HTTPClient.h>

#include <Adafruit_NeoPixel.h>

#include <stdlib.h>

#include <wiring_private.h>



#define N_LEDS 111

#define updateRate 500

#define PORT 80

#define BASE_URL "interlight-aarhus.herokuapp.com"



const char* ssid = "vilduhavevirus";

const char* password = "jadakommeddet";



int fsr = A0;

int fsrMin = 500;



int fsrTimeStamp;

int timeStamp;

int interval;

int postInterval;

int herokuTimeStamp;

int updateTimestamp;

int randomInterval;

int randomTimestamp;

int readTimeStamp;

int readInterval;



String getmoved;

String d1pos = "a";

String d2pos = "b";

String prevPos;

String getAngleAndBeenSet;



int fsrS1;



boolean isRunning;

boolean hasBeenTrue;

boolean upOrDown;

boolean isSetInHeroku;

boolean hasRun;



int actUp = D1;

int actDown = D3;



Adafruit_NeoPixel strip = Adafruit_NeoPixel(111, D6, NEO_GRB + NEO_KHZ800);



HTTPClient http;
void setup();
void loop();
void setInHeroku();
void fsrSignal();
void setHerokuData(int fsrSignal, String api_get);
void setServoandLED(int fsrSignal, String api_get);
void readFromHeroku(String angleAndSet);
void resetActuators();
void goDown();
void goUp();
void goNowhere();
static void chase(uint32_t c);
void wifiConnect();
void wifiCheck();
String post(String payload, String url);
String get(String url);
#line 99 "C:/Users/Tobias Lund Petersen/Documents/Arduino/arduino/src/fsr.ino"
void setup() {



  WiFi.persistent(false);



  delay(10);

  wifiConnect();



  Serial.begin(9600);



  timeStamp = 0;

  herokuTimeStamp = 0;

  interval = 1000;

  readInterval = 1500;

  readTimeStamp = millis() + 500;

  updateTimestamp = 0;

  randomTimestamp = 0;

  randomInterval = random(6000,15000);

  fsrS1 = 0;

  fsrTimeStamp = 0;

  isRunning = false;

  hasBeenTrue = false;

  upOrDown = true;

  isSetInHeroku = false;

  hasRun = false;



  getAngleAndBeenSet = "";

  prevPos = "";





  pinMode(actUp,OUTPUT);

  pinMode(actDown,OUTPUT);

  goNowhere();



  strip.begin();

  strip.setBrightness(200);

}



void loop() {

  wifiCheck();

  yield();





    setInHeroku();



  if(isSetInHeroku && !hasRun){

    Serial.println("yup");

    readFromHeroku(getAngleAndBeenSet);

  }



  if(!isSetInHeroku) {

    fsrSignal();

  }

  Serial.println(isSetInHeroku);

}



void setInHeroku(){

  getAngleAndBeenSet = get("/api/d1miniangle");





  String beenSet = getAngleAndBeenSet.substring(getAngleAndBeenSet.indexOf('-')+1);

  if(beenSet == "True" && !isSetInHeroku){

    Serial.print("yolo");

    isSetInHeroku = true;

  }

  if(isSetInHeroku && millis() > herokuTimeStamp + 10000){

    herokuTimeStamp = millis();

    if(beenSet == "False"){

      resetActuators();

    }

  }

}





void fsrSignal(){

  String api_get;



  if(millis() > fsrTimeStamp + 7000){

    fsrTimeStamp = millis();

    fsrS1 = analogRead(fsr);





  }
# 271 "C:/Users/Tobias Lund Petersen/Documents/Arduino/arduino/src/fsr.ino"
    api_get = get("/api/fsrgetvalue");



  setHerokuData(fsrS1, api_get);

  setServoandLED(fsrS1, api_get);

}





void setHerokuData(int fsrSignal, String api_get){

  String postString = "";
# 297 "C:/Users/Tobias Lund Petersen/Documents/Arduino/arduino/src/fsr.ino"
    if(fsrSignal>fsrMin){

      postString = "True" + d1pos;





    } else {

      postString = "False";

    }



      post(postString, "/api/fsrreadvalue");



    prevPos = api_get;



}



void setServoandLED(int fsrSignal, String api_get){

  String getmoved = api_get;

  boolean isSteppedUpon = false;



  if(fsrSignal < fsrMin){

    chase(strip.Color(0,80,0));

  } else if(fsrSignal > fsrMin){

    isSteppedUpon = true;

    chase(strip.Color(80, 0, 0));

  }



  if(getmoved == "True" + d2pos && d1pos == "a" || isSteppedUpon == true){

    hasBeenTrue = true;

  }



  if(millis() > randomTimestamp + randomInterval){

    randomTimestamp = millis();





    randomInterval = random(3000,12000);
# 373 "C:/Users/Tobias Lund Petersen/Documents/Arduino/arduino/src/fsr.ino"
    if(isRunning == false && hasBeenTrue == true){

      hasBeenTrue = false;

      isRunning = true;





      if(upOrDown){

        upOrDown = !upOrDown;

        goDown();

      } else if(!upOrDown) {

        upOrDown = !upOrDown;

        goUp();

      }

    } else {

      isRunning = false;

      goNowhere();

    }

  }

}





void readFromHeroku(String angleAndSet){



  String getangle = angleAndSet.substring(0, angleAndSet.indexOf('-'));



  int angle = getangle.toInt();

  int convertToMs = (angle*1000)/3;

  if(millis() > timeStamp + convertToMs && d1pos == "a"){

    timeStamp = millis();

    if(isRunning == false){

      isRunning = true;

      goUp();

    } else if(isRunning == true){

      isRunning = false;

      goNowhere();

      hasRun = true;

    }

  }

}



void resetActuators(){

  if(millis() > readTimeStamp + 14000){

    readTimeStamp = millis();

    if(!isRunning){

      goDown();

    } else if(isRunning){

      goNowhere();

      isSetInHeroku = false;

    }

  }



}



void goDown(){

  digitalWrite(actDown, LOW);

  digitalWrite(actUp, HIGH);

}



void goUp(){

  digitalWrite(actDown, HIGH);

  digitalWrite(actUp, LOW);

}



void goNowhere(){

  digitalWrite(actDown, HIGH);

  digitalWrite(actUp, HIGH);

}





static void chase(uint32_t c) {

 for(uint16_t i=0; i<N_LEDS; i++) {

     strip.setPixelColor(i , c);

     strip.show();

 }

}





void wifiConnect() {





  Serial.println();

  Serial.println();

  Serial.print("Connecting to ");

  Serial.println(ssid);



  WiFi.begin(ssid, password);



  while (WiFi.status() != WL_CONNECTED) {

    delay(500);

    Serial.print(".");

  }



  Serial.println("");

  Serial.println("WiFi connected");

  Serial.println("IP address: ");

  Serial.println(WiFi.localIP());

}



void wifiCheck(){

  if (WiFi.status() != WL_CONNECTED)

  {

    int tickCount = 0;

    Serial.println("Wifi dropped. Retry in 60 seconds.");

    delay(60000);

    Serial.println("Connecting");

    WiFi.begin(ssid, password);



    while (WiFi.status() != WL_CONNECTED) {

      delay(500);

      Serial.println(".");

      tickCount++;

      if (tickCount > 100)

      {

        Serial.println("Wifi fail...");



        while (1);

      }

    }







  }

}



String post(String payload, String url) {

    Serial.println("[POST BEGIN] ...");

    String response = "Request not allowed because of timelimit";

    if( millis() - updateTimestamp > updateRate) {

      updateTimestamp = millis();





      http.begin(BASE_URL, PORT, url);

      Serial.println("[POST] payload: " + payload);

      Serial.println("url: " + url);

      String status = String(http.POST(payload));

      response = http.getString();



      Serial.println("Response: " + response);

      http.end();

    }

    return response;

}



String get(String url) {

    Serial.println("[GET BEGIN] ...");

    String response = "Request not allowed because of timelimit";

    if( millis() - updateTimestamp > updateRate) {

        http.begin(BASE_URL, PORT, url);



        int httpCode = http.GET();



        if(httpCode > 0) {
# 683 "C:/Users/Tobias Lund Petersen/Documents/Arduino/arduino/src/fsr.ino"
            if(httpCode == HTTP_CODE_OK) {

                response = http.getString();

                Serial.println("[HTTP] Response " + response);

            }

        } else {

            Serial.println("[HTTP] GET... failed, error: " + String(httpCode));

            response = "ERROR";

        }

        http.end();

      }

      return response;

}